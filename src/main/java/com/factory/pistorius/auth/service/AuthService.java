package com.factory.pistorius.auth.service;

import com.factory.pistorius.auth.domain.request.SigninRequest;
import com.factory.pistorius.auth.domain.request.SignupRequest;
import com.factory.pistorius.auth.domain.response.MessageResponse;
import com.factory.pistorius.auth.domain.response.SigninResponse;

public interface AuthService {
  SigninResponse getSigninResponse(SigninRequest signinRequest);

  MessageResponse registerUser(SignupRequest signupRequest);

}
