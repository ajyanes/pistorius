package com.factory.pistorius.auth.service.impl;

import com.factory.pistorius.auth.domain.ERole;
import com.factory.pistorius.auth.domain.Role;
import com.factory.pistorius.auth.domain.User;
import com.factory.pistorius.auth.domain.request.SigninRequest;
import com.factory.pistorius.auth.domain.request.SignupRequest;
import com.factory.pistorius.auth.domain.response.MessageResponse;
import com.factory.pistorius.auth.domain.response.SigninResponse;
import com.factory.pistorius.auth.repository.RoleRepository;
import com.factory.pistorius.auth.repository.UserRepository;
import com.factory.pistorius.auth.service.AuthService;
import com.factory.pistorius.auth.service.UserDetailsImpl;
import com.factory.pistorius.security.JwtUtils;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class AuthServiceImpl implements AuthService {

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  JwtUtils jwtUtils;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder encoder;

  @Override
  public SigninResponse getSigninResponse(SigninRequest signinRequest) {
    SigninResponse signinResponse = new SigninResponse();

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(signinRequest.getUsername(), signinRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String token = jwtUtils.generateJwtToken(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    signinResponse.setToken(token);
    signinResponse.setUserDetails(userDetails);
    signinResponse.setRoles(roles);

    return signinResponse;
  }

  @Override
  public MessageResponse registerUser(SignupRequest signupRequest) {
    if(!validateUsername(signupRequest)){
      new MessageResponse("Error: Username is already taken!");
    }

    if(!validateEmail(signupRequest)){
      new MessageResponse("Error: Username is already taken!");
    }

    // Create new user's account
    User user = new User(signupRequest.getUsername(),
        signupRequest.getEmail(),
        encoder.encode(signupRequest.getPassword()));

    Set<String> strRoles = signupRequest.getRole();
    Set<Role> roles = getRoles(strRoles);

    user.setRoles(roles);
    try {
      userRepository.save(user);
    }catch (Exception e){
      new MessageResponse("Error registrando usuario "+e.getMessage());
    }

    return new MessageResponse("User registered successfully!");
  }

  private boolean validateUsername(SignupRequest signUpRequest) {
    if (userRepository.existsByUsername(signUpRequest.getUsername())) {
      return true;
    } else {
      return false;
    }
  }
  private boolean validateEmail(SignupRequest signUpRequest){
    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      return true;
    }else{
      return false;
    }
  }
  private Set<Role> getRoles(Set<String> stringSet){
    Set<Role> roles = new HashSet<>();

    if (stringSet == null) {
      Role userRole = roleRepository.findByName(ERole.ROLE_USER)
          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(userRole);
    } else {
      stringSet.forEach(role -> {
        switch (role) {
          case "admin":
            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);

            break;
          case "mod":
            Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(modRole);

            break;
          default:
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
      });
    }

    return roles;
  }

}
