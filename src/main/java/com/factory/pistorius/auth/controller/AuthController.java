package com.factory.pistorius.auth.controller;

import com.factory.pistorius.auth.domain.request.SigninRequest;
import com.factory.pistorius.auth.domain.request.SignupRequest;
import com.factory.pistorius.auth.domain.response.JwtResponse;
import com.factory.pistorius.auth.domain.response.MessageResponse;
import com.factory.pistorius.auth.domain.response.SigninResponse;
import com.factory.pistorius.auth.service.AuthService;
import com.factory.pistorius.auth.service.UserDetailsImpl;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/app")
public class AuthController {

  @Autowired
  private AuthService authService;

  @PostMapping("/login")
  public ResponseEntity<?> login(@Valid @RequestBody SigninRequest signinRequest) {
    SigninResponse response = authService.getSigninResponse(signinRequest);

    UserDetailsImpl userDetails = response.getUserDetails();

    return ResponseEntity.ok(new JwtResponse(response.getToken(),
        userDetails.getId(),
        userDetails.getUsername(),
        userDetails.getEmail(),
        response.getRoles()));
  }

  @PostMapping("/signup")
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

    MessageResponse messageResponse = authService.registerUser(signUpRequest);

    return ResponseEntity.ok(messageResponse);
  }


}
