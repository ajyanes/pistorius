package com.factory.pistorius.auth.domain.response;

import com.factory.pistorius.auth.service.UserDetailsImpl;
import java.util.List;

public class SigninResponse {

  private String token;
  private UserDetailsImpl userDetails;
  private List<String> roles;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public UserDetailsImpl getUserDetails() {
    return userDetails;
  }

  public void setUserDetails(UserDetailsImpl userDetails) {
    this.userDetails = userDetails;
  }

  public List<String> getRoles() {
    return roles;
  }

  public void setRoles(List<String> roles) {
    this.roles = roles;
  }
}
