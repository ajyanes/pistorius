package com.factory.pistorius.invoices.service.impl;

import com.factory.pistorius.Util.MessageResource;
import com.factory.pistorius.Util.StateEnum;
import com.factory.pistorius.customers.domain.mapper.CustomerMapper;
import com.factory.pistorius.exceptions.InvoiceException;
import com.factory.pistorius.invoices.domain.Invoice;
import com.factory.pistorius.invoices.domain.InvoiceDetail;
import com.factory.pistorius.invoices.domain.dto.InvoiceFindResponseDto;
import com.factory.pistorius.invoices.domain.dto.InvoiceRequestDto;
import com.factory.pistorius.invoices.domain.dto.InvoiceResponseDto;
import com.factory.pistorius.invoices.mappers.InvoiceMapper;
import com.factory.pistorius.invoices.repository.InvoiceDetailRepository;
import com.factory.pistorius.invoices.repository.InvoiceRepository;
import com.factory.pistorius.invoices.service.InvoiceService;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class InvoiceServiceImpl implements InvoiceService {

  @Autowired
  private InvoiceRepository invoiceRepository;
  @Autowired
  private InvoiceDetailRepository invoiceDetailRepository;

  InvoiceMapper invoiceMapper = Mappers.getMapper(InvoiceMapper.class);

  @Override
  public InvoiceResponseDto createInvoice(InvoiceRequestDto invoiceRequestDto) {
    validationsInvoice(invoiceRequestDto);

    try {
      Invoice invoice = getInvoice(invoiceRequestDto);
      Invoice invoiceSaved = invoiceRepository.save(invoice);

      for (InvoiceDetail detail : invoiceRequestDto.getInvoiceDetail()) {
        invoiceDetailRepository.save(getDetail(detail, invoiceSaved.getNumero()));
      }

    return InvoiceResponseDto.builder().invoice(invoiceSaved).build();

    }catch (Exception exception){
      throw new InvoiceException("Error al guardar registro "+exception.getMessage());
    }
  }

  @Override
  public InvoiceFindResponseDto findInvoice(String id) {
    InvoiceFindResponseDto invoiceFindResponseDto =
        invoiceMapper.fromEntity(invoiceRepository.findInvoiceByNumero(id),
            invoiceDetailRepository.findInvoiceDetailByNumeroFactura(id));

    return invoiceFindResponseDto;
  }

  private void validationsInvoice(InvoiceRequestDto invoiceRequestDto) {
    if(CollectionUtils.isEmpty(invoiceRequestDto.getInvoiceDetail())){
      throw new InvoiceException(MessageResource.DETAIL_IS_EMPTY.getName());
    }
    if(!validateTotals(invoiceRequestDto)){
      throw new InvoiceException(MessageResource.DIFFERENT_TOTALS.getName());
    }
    if(!validateCant(invoiceRequestDto.getInvoiceDetail())){
      throw new InvoiceException(MessageResource.DETAIL_CANT_IS_ZERO.getName());
    }
  }

  public Invoice getInvoice(InvoiceRequestDto invoiceRequestDto){
    Invoice invoice = new Invoice();
    invoice.setBodega(invoiceRequestDto.getInvoice().getBodega());
    invoice.setConcepto(invoiceRequestDto.getInvoice().getConcepto());
    invoice.setNumero(invoiceRequestDto.getInvoice().getNumero());
    invoice.setFecha(LocalDateTime.now());
    invoice.setValor(invoiceRequestDto.getInvoice().getValor());
    invoice.setSaldo(invoiceRequestDto.getInvoice().getSaldo());
    invoice.setCodCliente(invoiceRequestDto.getInvoice().getCodCliente());
    invoice.setEstado(StateEnum.ACTIVA.name());

    return invoice;
  }

  public InvoiceDetail getDetail(InvoiceDetail detail, String invoiceNum){
    InvoiceDetail invoiceDetail = new InvoiceDetail();
    invoiceDetail.setCodProducto(detail.getCodProducto());
    invoiceDetail.setCantidad(detail.getCantidad());
    invoiceDetail.setNumeroFactura(invoiceNum);
    invoiceDetail.setValorTotal(detail.getValorTotal());
    invoiceDetail.setValorUnitario(detail.getValorUnitario());
    invoiceDetail.setPorcDescuento(detail.getPorcDescuento());
    invoiceDetail.setValorDescuento(detail.getValorDescuento());

    return invoiceDetail;
  }

  private boolean validateTotals(InvoiceRequestDto invoiceRequestDto){
    BigDecimal totalHeader = getInvoice(invoiceRequestDto).getValor();
    BigDecimal totalDetail = BigDecimal.ZERO;

    for (InvoiceDetail detail : invoiceRequestDto.getInvoiceDetail()) {
      totalDetail = totalDetail.add(detail.getValorTotal());
    }

    return totalHeader.compareTo(totalDetail) == 0;

  }

  private boolean validateCant(List<InvoiceDetail> invoiceDetails){
    for (InvoiceDetail detail : invoiceDetails) {
      if(detail.getCantidad() == 0){
        return false;
      }
    }
    return true;
  }

}
