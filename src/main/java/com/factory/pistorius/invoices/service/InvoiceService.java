package com.factory.pistorius.invoices.service;

import com.factory.pistorius.invoices.domain.dto.InvoiceFindResponseDto;
import com.factory.pistorius.invoices.domain.dto.InvoiceRequestDto;
import com.factory.pistorius.invoices.domain.dto.InvoiceResponseDto;

public interface InvoiceService {

  InvoiceResponseDto createInvoice(InvoiceRequestDto invoiceRequestDto);

  InvoiceFindResponseDto findInvoice(String id);
}
