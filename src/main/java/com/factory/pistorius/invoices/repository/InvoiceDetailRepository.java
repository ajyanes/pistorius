package com.factory.pistorius.invoices.repository;

import com.factory.pistorius.invoices.domain.InvoiceDetail;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceDetailRepository extends CrudRepository<InvoiceDetail, Long> {

  List<InvoiceDetail> findInvoiceDetailByNumeroFactura(String id);

}
