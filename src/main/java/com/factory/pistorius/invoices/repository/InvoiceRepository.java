package com.factory.pistorius.invoices.repository;

import com.factory.pistorius.invoices.domain.Invoice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

  Invoice findInvoiceByNumero(String id);
}
