package com.factory.pistorius.invoices.mappers;

import com.factory.pistorius.invoices.domain.Invoice;
import com.factory.pistorius.invoices.domain.InvoiceDetail;
import com.factory.pistorius.invoices.domain.dto.InvoiceFindResponseDto;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper
public interface InvoiceMapper {

  InvoiceFindResponseDto fromEntity(Invoice invoice, List<InvoiceDetail> invoiceDetail);

}
