package com.factory.pistorius.invoices.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "factura",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = "numero")
    })
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

  @Id
  private String numero;
  private String concepto;
  private BigDecimal valor;
  private BigDecimal saldo;
  private LocalDateTime fecha;
  private String estado;
  private String codCliente;
  private int bodega;

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public String getConcepto() {
    return concepto;
  }

  public void setConcepto(String concepto) {
    this.concepto = concepto;
  }

  public BigDecimal getValor() {
    return valor;
  }

  public void setValor(BigDecimal valor) {
    this.valor = valor;
  }

  public BigDecimal getSaldo() {
    return saldo;
  }

  public void setSaldo(BigDecimal saldo) {
    this.saldo = saldo;
  }

  public LocalDateTime getFecha() {
    return fecha;
  }

  public void setFecha(LocalDateTime fecha) {
    this.fecha = fecha;
  }

  public String getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = estado;
  }

  public String getCodCliente() {
    return codCliente;
  }

  public void setCodCliente(String codCliente) {
    this.codCliente = codCliente;
  }

  public int getBodega() {
    return bodega;
  }

  public void setBodega(int bodega) {
    this.bodega = bodega;
  }
}
