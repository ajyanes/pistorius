package com.factory.pistorius.invoices.domain.dto;

import com.factory.pistorius.invoices.domain.Invoice;
import com.factory.pistorius.invoices.domain.InvoiceDetail;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InvoiceRequestDto {

  private Invoice invoice;
  private List<InvoiceDetail> invoiceDetail;

}
