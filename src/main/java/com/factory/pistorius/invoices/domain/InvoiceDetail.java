package com.factory.pistorius.invoices.domain;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "detalleFactura",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")
    })
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDetail {

  @Id
  private int id;
  private String numeroFactura;
  private String codProducto;
  private int cantidad;
  private BigDecimal valorUnitario;
  private BigDecimal valorTotal;
  private int porcDescuento;
  private BigDecimal valorDescuento;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNumeroFactura() {
    return numeroFactura;
  }

  public void setNumeroFactura(String numeroFactura) {
    this.numeroFactura = numeroFactura;
  }

  public String getCodProducto() {
    return codProducto;
  }

  public void setCodProducto(String codProducto) {
    this.codProducto = codProducto;
  }

  public int getCantidad() {
    return cantidad;
  }

  public void setCantidad(int cantidad) {
    this.cantidad = cantidad;
  }

  public BigDecimal getValorUnitario() {
    return valorUnitario;
  }

  public void setValorUnitario(BigDecimal valorUnitario) {
    this.valorUnitario = valorUnitario;
  }

  public BigDecimal getValorTotal() {
    return valorTotal;
  }

  public void setValorTotal(BigDecimal valorTotal) {
    this.valorTotal = valorTotal;
  }

  public int getPorcDescuento() {
    return porcDescuento;
  }

  public void setPorcDescuento(int porcDescuento) {
    this.porcDescuento = porcDescuento;
  }

  public BigDecimal getValorDescuento() {
    return valorDescuento;
  }

  public void setValorDescuento(BigDecimal valorDescuento) {
    this.valorDescuento = valorDescuento;
  }
}
