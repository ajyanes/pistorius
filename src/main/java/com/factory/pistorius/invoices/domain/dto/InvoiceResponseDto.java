package com.factory.pistorius.invoices.domain.dto;

import com.factory.pistorius.invoices.domain.Invoice;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InvoiceResponseDto {

  private Invoice invoice;

}
