package com.factory.pistorius.invoices.controller;

import com.factory.pistorius.Util.Response;
import com.factory.pistorius.auth.domain.response.MessageResponse;
import com.factory.pistorius.invoices.domain.dto.InvoiceFindResponseDto;
import com.factory.pistorius.invoices.domain.dto.InvoiceRequestDto;
import com.factory.pistorius.invoices.domain.dto.InvoiceResponseDto;
import com.factory.pistorius.invoices.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/invoice")
public class InvoiceController {

  @Autowired
  private InvoiceService invoiceService;

  @PostMapping
  public ResponseEntity<?> saveInvoice(@RequestBody InvoiceRequestDto invoiceRequestDto){
    InvoiceResponseDto invoiceResponseDto = invoiceService.createInvoice(invoiceRequestDto);

    if(invoiceResponseDto==null){
      return new ResponseEntity<>(new MessageResponse("Registro fallido"),HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(Response.builder()
        .code(HttpStatus.CREATED.name())
        .data(invoiceResponseDto)
        .build(),HttpStatus.CREATED);
  }

  @GetMapping("/id/{id}")
  public ResponseEntity<?> getInvoice(@PathVariable("id")  String id){
    InvoiceFindResponseDto invoiceFindResponseDto = invoiceService.findInvoice(id);

    if(invoiceFindResponseDto==null){
      return new ResponseEntity<>(new MessageResponse("Registro no encontrado "+id),HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(Response.builder()
        .code(HttpStatus.OK.name())
        .data(invoiceFindResponseDto)
        .build(),HttpStatus.OK);
  }
}
