package com.factory.pistorius.products.service.Impl;

import com.factory.pistorius.products.domain.Product;
import com.factory.pistorius.products.domain.dto.ProductDto;
import com.factory.pistorius.products.domain.mapper.ProductMapper;
import com.factory.pistorius.products.repository.ProductRepository;
import com.factory.pistorius.products.service.ProductService;
import java.util.List;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductRepository productRepository;

  ProductMapper productMapper = Mappers.getMapper(ProductMapper.class);

  @Override
  public List<Product> getAllProductByName(String name) {
    return productRepository.findProductsByNombreContains(name);
  }

  @Override
  public Product getProductByCod(String cod) {
    return productRepository.findProductByCod(cod);
  }

  @Override
  public ProductDto save(ProductDto productDto) {
    Product product = productRepository.save(productMapper.fromDto(productDto));

    return productMapper.toDto(product);
  }

  @Override
  public ProductDto updateProduct(ProductDto productDto) {
    Product product = productRepository.findProductByCod(productDto.getCod());

    if(product==null){
      return new ProductDto();
    }else{
      product = productRepository.save(productMapper.fromDto(productDto));
    }

    return productMapper.toDto(product);
  }

}
