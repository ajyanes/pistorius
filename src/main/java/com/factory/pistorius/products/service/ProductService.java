package com.factory.pistorius.products.service;

import com.factory.pistorius.products.domain.Product;
import com.factory.pistorius.products.domain.dto.ProductDto;
import java.util.List;

public interface ProductService {

  List<Product> getAllProductByName(String name);
  Product getProductByCod(String cod);
  ProductDto save(ProductDto productDto);
  ProductDto updateProduct(ProductDto productDto);

}
