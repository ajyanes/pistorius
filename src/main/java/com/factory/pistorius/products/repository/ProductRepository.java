package com.factory.pistorius.products.repository;

import com.factory.pistorius.products.domain.Product;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

  Product findProductByCod(String cod);
  List<Product> findProductsByNombreContains(String name);

}
