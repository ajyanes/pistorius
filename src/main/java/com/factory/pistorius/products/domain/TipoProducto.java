package com.factory.pistorius.products.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipos_producto")
public class TipoProducto {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id_tipo;
  private String nombreTipo;

  public Long getId_tipo() {
    return id_tipo;
  }

  public void setId_tipo(Long id_tipo) {
    this.id_tipo = id_tipo;
  }

  public String getNombreTipo() {
    return nombreTipo;
  }

  public void setNombreTipo(String nombreTipo) {
    this.nombreTipo = nombreTipo;
  }
}
