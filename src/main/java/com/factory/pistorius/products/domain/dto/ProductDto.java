package com.factory.pistorius.products.domain.dto;

import com.factory.pistorius.auth.domain.response.MessageResponse;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

  private String cod;
  private String nombre;
  private String descrip;
  private String referencia;
  private LocalDate fechatiempo;
  private Long tipo;
  private MessageResponse message;

  public String getCod() {
    return cod;
  }

  public void setCod(String cod) {
    this.cod = cod;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getDescrip() {
    return descrip;
  }

  public void setDescrip(String descrip) {
    this.descrip = descrip;
  }

  public String getReferencia() {
    return referencia;
  }

  public void setReferencia(String referencia) {
    this.referencia = referencia;
  }

  public LocalDate getFechatiempo() {
    return fechatiempo;
  }

  public void setFechatiempo(LocalDate fechatiempo) {
    this.fechatiempo = fechatiempo;
  }

  public Long getTipo() { return tipo; }

  public void setTipo(Long tipo) {
    this.tipo = tipo;
  }

  public MessageResponse getMessage() {
    return message;
  }

  public void setMessage(MessageResponse message) {
    this.message = message;
  }
}
