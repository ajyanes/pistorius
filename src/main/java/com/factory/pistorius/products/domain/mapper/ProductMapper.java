package com.factory.pistorius.products.domain.mapper;

import com.factory.pistorius.products.domain.Product;
import com.factory.pistorius.products.domain.dto.ProductDto;
import org.mapstruct.Mapper;

@Mapper
public interface ProductMapper {

  Product fromDto(ProductDto productDto);

  ProductDto toDto(Product product);

}
