package com.factory.pistorius.products.controller;

import com.factory.pistorius.auth.domain.response.MessageResponse;
import com.factory.pistorius.products.domain.Product;
import com.factory.pistorius.products.domain.dto.ProductDto;
import com.factory.pistorius.products.service.ProductService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/product")
public class ProductController {

  @Autowired
  private ProductService productService;

  @GetMapping("/name/{name}")
  public ResponseEntity<?> getProductByName(@PathVariable("name") String name){
    List<Product> products = productService.getAllProductByName(name);

    if(products.isEmpty()){
      return new ResponseEntity<>(new MessageResponse("No se encontraron resultados"),HttpStatus.BAD_REQUEST);
    }else {
      return new ResponseEntity<>(products, HttpStatus.OK);
    }
  }

  @GetMapping("/cod/{cod}")
  public ResponseEntity<?> getProductByCod(@PathVariable("cod") String cod){
    Product product = productService.getProductByCod(cod);

    if(product==null){
      return new ResponseEntity<>(new MessageResponse("No se encontro Producto con el código "+cod),HttpStatus.BAD_REQUEST);
    }else {
      return new ResponseEntity<>(product, HttpStatus.OK);
    }
  }

  @PostMapping
  public ResponseEntity<?> saveProduct(@RequestBody ProductDto productDto){
    ProductDto dto = productService.save(productDto);

    if(dto!=null){
      dto.setMessage(new MessageResponse("Registro Exitoso!!"));
      return new ResponseEntity<>(dto, HttpStatus.OK);
    }else{
      return new ResponseEntity<>(new MessageResponse("No se encontraron resultados"),HttpStatus.BAD_REQUEST);
    }
  }

  @PutMapping
  public ResponseEntity<?> updateProduct(@RequestBody ProductDto productDto){
    ProductDto dto = productService.updateProduct(productDto);

    if(dto.getCod()!=null){
      dto.setMessage(new MessageResponse("Registro Actualizado!!"));
      return new ResponseEntity<>(dto, HttpStatus.OK);
    }else{
      return new ResponseEntity<>(new MessageResponse("No se encontro el  producto con codigo "+productDto.getCod()),HttpStatus.BAD_REQUEST);
    }
  }

}
