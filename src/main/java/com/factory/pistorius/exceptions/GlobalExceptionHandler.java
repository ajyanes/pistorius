package com.factory.pistorius.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(value = {InvoiceException.class})
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorMessage resourceNotFoundException(InvoiceException invoiceException) {
    ErrorMessage message = new ErrorMessage(
        HttpStatus.BAD_REQUEST.value(),
        HttpStatus.BAD_REQUEST.name(),
        invoiceException.getMessage());

    return message;
  }

}
