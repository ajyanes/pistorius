package com.factory.pistorius.exceptions;

public class InvoiceException extends RuntimeException {


  public InvoiceException(String messageResource) {
    super(messageResource);
  }

}
