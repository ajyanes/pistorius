package com.factory.pistorius.Util.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "zonas",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = "id_zona")
    })
@AllArgsConstructor
@NoArgsConstructor
public class Zona {

  @Id
  private int id_zona;
  private String nombreZona;

  public int getId_zona() {
    return id_zona;
  }

  public void setId_zona(int id_zona) {
    this.id_zona = id_zona;
  }

  public String getNombreZona() {
    return nombreZona;
  }

  public void setNombreZona(String nombreZona) {
    this.nombreZona = nombreZona;
  }
}
