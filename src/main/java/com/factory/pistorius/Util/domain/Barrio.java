package com.factory.pistorius.Util.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "barrios",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = "id_barrio")
    })
@AllArgsConstructor
@NoArgsConstructor
public class Barrio {

  @Id
  private int id_barrio;
  private String nombreBarrio;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinTable(  name = "zonas",
      joinColumns = @JoinColumn(name = "id_zona"))
  private Zona zona;

  public int getIdBarrio() {
    return id_barrio;
  }

  public void setId_barrio(int id_barrio) {
    this.id_barrio = id_barrio;
  }

  public String getNombreBarrio() {
    return nombreBarrio;
  }

  public void setNombreBarrio(String nombreBarrio) {
    this.nombreBarrio = nombreBarrio;
  }

  public Zona getZona() {
    return zona;
  }

  public void setZona(Zona zona) {
    this.zona = zona;
  }
}
