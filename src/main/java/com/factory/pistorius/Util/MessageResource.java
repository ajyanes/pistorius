package com.factory.pistorius.Util;

public enum MessageResource {

  DIFFERENT_TOTALS("El valor de la factura es diferente a la suma del detalle"),
  DETAIL_IS_EMPTY("Debe adicionar por lo menos un item al detalle de la factura"),
  DETAIL_CANT_IS_ZERO("Hay registros con cantidad 0 en el detalle");

  private String name;

  MessageResource(String name) {
    this.name = name;
  }

  public String getName(){
    return name;
  }

}
