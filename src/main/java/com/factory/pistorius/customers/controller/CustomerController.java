package com.factory.pistorius.customers.controller;

import com.factory.pistorius.auth.domain.response.MessageResponse;
import com.factory.pistorius.customers.domain.dto.CustomerDto;
import com.factory.pistorius.customers.domain.dto.CustomerResponseDto;
import com.factory.pistorius.customers.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/customer")
public class CustomerController {

  @Autowired
  private CustomerService customerService;

  @PostMapping
  public ResponseEntity<?> saveCustomer(@RequestBody CustomerDto customer){
    CustomerDto response = customerService.save(customer);

    if(response!=null){
      response.setMessage(new MessageResponse("Registro Exitoso!!"));
      return new ResponseEntity<>(response, HttpStatus.OK);
    }else{
      return new ResponseEntity<>(new MessageResponse("No se pudo guardar el registro"),HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping("/findByName/{name}")
  public ResponseEntity<CustomerResponseDto> findByName(@PathVariable("name") String name){
    CustomerResponseDto customerResponseDto = new CustomerResponseDto();
    customerResponseDto.setCustomerDtoList(customerService.findByName(name));

    if(!customerResponseDto.getCustomerDtoList().isEmpty()){
      customerResponseDto.setMessage(new MessageResponse(customerResponseDto.getCustomerDtoList().size()+" registros encontrado (s)"));
      return new ResponseEntity<>(customerResponseDto, HttpStatus.OK);
    }else{
      customerResponseDto.setMessage(new MessageResponse("No se pudo guardar el registro"));
      return new ResponseEntity<>(customerResponseDto,HttpStatus.BAD_REQUEST);
    }
  }

}
