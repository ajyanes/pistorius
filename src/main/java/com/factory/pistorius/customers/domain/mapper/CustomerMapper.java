package com.factory.pistorius.customers.domain.mapper;

import com.factory.pistorius.customers.domain.Customer;
import com.factory.pistorius.customers.domain.dto.CustomerDto;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {

  Customer fromDto(CustomerDto customerDto);
  CustomerDto fromEntity(Customer customer);
  List<Customer> fromListDto(List<CustomerDto> customerDtos);
  List<CustomerDto> fromListEntity(List<Customer> customers);
}
