package com.factory.pistorius.customers.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "clientes",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = "nit")
    })
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

  @Id
  private String nit;
  private String nombre;
  private String apellido;
  private String direccion;
  private String telefono;
  private String movil;
  private String plazo;
  private String contacto;
  private int id_barrio;


  public String getNit() {
    return nit;
  }

  public void setNit(String nit) {
    this.nit = nit;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getDireccion() {
    return direccion;
  }

  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public String getMovil() {
    return movil;
  }

  public void setMovil(String movil) {
    this.movil = movil;
  }

  public String getPlazo() {
    return plazo;
  }

  public void setPlazo(String plazo) {
    this.plazo = plazo;
  }

  public String getContacto() {
    return contacto;
  }

  public void setContacto(String contacto) {
    this.contacto = contacto;
  }

  public int getId_barrio() {
    return id_barrio;
  }

  public void setId_barrio(int id_barrio) {
    this.id_barrio = id_barrio;
  }
}
