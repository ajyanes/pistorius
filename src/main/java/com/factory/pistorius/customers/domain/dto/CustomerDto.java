package com.factory.pistorius.customers.domain.dto;

import com.factory.pistorius.auth.domain.response.MessageResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {

  private String nit;
  private String nombre;
  private String apellido;
  private String direccion;
  private String telefono;
  private String movil;
  private String plazo;
  private String contacto;
  private int id_barrio;
  private MessageResponse message;

  public String getNit() {
    return nit;
  }

  public void setNit(String nit) {
    this.nit = nit;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getDireccion() {
    return direccion;
  }

  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public String getMovil() {
    return movil;
  }

  public void setMovil(String movil) {
    this.movil = movil;
  }

  public String getPlazo() {
    return plazo;
  }

  public void setPlazo(String plazo) {
    this.plazo = plazo;
  }

  public String getContacto() {
    return contacto;
  }

  public void setContacto(String contacto) {
    this.contacto = contacto;
  }

  public int getId_barrio() {
    return id_barrio;
  }

  public void setId_barrio(int id_barrio) {
    this.id_barrio = id_barrio;
  }

  public MessageResponse getMessage() {
    return message;
  }

  public void setMessage(MessageResponse message) {
    this.message = message;
  }
}
