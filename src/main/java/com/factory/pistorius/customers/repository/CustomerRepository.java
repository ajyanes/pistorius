package com.factory.pistorius.customers.repository;

import com.factory.pistorius.customers.domain.Customer;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findAllByNombreContainingIgnoreCase(String name);
}
