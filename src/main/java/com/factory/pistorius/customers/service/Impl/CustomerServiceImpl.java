package com.factory.pistorius.customers.service.Impl;

import com.factory.pistorius.customers.domain.Customer;
import com.factory.pistorius.customers.domain.dto.CustomerDto;
import com.factory.pistorius.customers.domain.mapper.CustomerMapper;
import com.factory.pistorius.customers.repository.CustomerRepository;
import com.factory.pistorius.customers.service.CustomerService;
import java.util.List;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  CustomerMapper customerMapper = Mappers.getMapper(CustomerMapper.class);

  @Override
  public CustomerDto save(CustomerDto customerDto) {
    Customer customer = customerRepository.save(customerMapper.fromDto(customerDto));

    return customerMapper.fromEntity(customer);
  }

  @Override
  public List<CustomerDto> findByName(String name) {
    List<Customer> customers = customerRepository.findAllByNombreContainingIgnoreCase(name);

    return customerMapper.fromListEntity(customers);
  }

}
