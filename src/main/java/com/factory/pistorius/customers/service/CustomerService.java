package com.factory.pistorius.customers.service;

import com.factory.pistorius.customers.domain.dto.CustomerDto;
import java.util.List;

public interface CustomerService {

  CustomerDto save(CustomerDto customer);

  List<CustomerDto> findByName(String name);

}
