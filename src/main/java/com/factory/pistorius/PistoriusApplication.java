package com.factory.pistorius;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PistoriusApplication {

  public static void main(String[] args) {
    SpringApplication.run(PistoriusApplication.class, args);
  }

}
